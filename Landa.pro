QT += quick

CONFIG += c++11
CONFIG+=debug
CONFIG+=qml_debug

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

QT += location positioning

android {
    QT += androidextras
    }

SOURCES += \
        main.cpp \
        src/controllers/BaseController.cpp \
        src/controllers/AuthenticationController.cpp \
        src/controllers/PermissionController.cpp \
        src/datasets/BaseDataset.cpp

RESOURCES += qml.qrc

INCLUDEPATH += src/

# Additional import path used to resolve QML modules in Qt Creator's code model
# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
  src/controllers/BaseController.h \
  src/controllers/AuthenticationController.h \
  src/controllers/PermissionController.h \
  src/datasets/BaseDataset.h

ANDROID_ABIS = armeabi-v7a arm64-v8a x86 x86_64

DISTFILES +=
