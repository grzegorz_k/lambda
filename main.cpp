#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QSettings>
#include <controllers/AuthenticationController.h>
#include <controllers/PermissionController.h>
int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QSettings *loginSettings = new QSettings("ScytheStudio","Landa");
    AuthenticationController authController(loginSettings);
    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("authController",&authController);
    PermissionController permissonController(&engine);
    engine.rootContext()->setContextProperty("permissonController", &permissonController);
    const QUrl url(QStringLiteral("qrc:/qml/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
