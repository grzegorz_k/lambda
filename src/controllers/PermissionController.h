#ifndef PERMISSIONCONTROLLER_H
#define PERMISSIONCONTROLLER_H

#include <QObject>
#include <QDebug>

#ifdef Q_OS_ANDROID
#include <QtAndroid>
#endif

class PermissionController : public QObject
{
    Q_OBJECT
public:
    explicit PermissionController(QObject *parent = nullptr);
    Q_INVOKABLE void showGpsPermissionDialog();
    Q_INVOKABLE void showNotificationPermissionDialog();
signals:

};

#endif // PERMISSIONCONTROLLER_H
