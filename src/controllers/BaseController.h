#ifndef BASECONTROLLER_H
#define BASECONTROLLER_H

#include <QObject>
#include <QSettings>

class BaseController : public QObject
{
    Q_OBJECT
public:
    explicit BaseController(QSettings *loginSettings,QObject *parent = nullptr);
    QSettings *loginSettings() const;

signals:
private:
    QSettings *_loginSettings = nullptr;
};

#endif // BASECONTROLLER_H
