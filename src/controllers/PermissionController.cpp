#include "PermissionController.h"

PermissionController::PermissionController(QObject *parent) : QObject(parent)
{

}

void PermissionController::showGpsPermissionDialog()
{
 #ifdef Q_OS_ANDROID
    QtAndroid::PermissionResult r = QtAndroid::checkPermission("android.permission.ACCESS_FINE_LOCATION");
       if(r == QtAndroid::PermissionResult::Denied) {
         QtAndroid::requestPermissionsSync( QStringList() << "android.permission.ACCESS_FINE_LOCATION" );
         qDebug() << "Fine_Location Denied";
    }
 #else
    qDebug() << "PERMISSIONCONTROLLER: Platform not supported for this action!";
#endif
}

void PermissionController::showNotificationPermissionDialog()
{
 #ifdef Q_OS_ANDROID
    QtAndroid::PermissionResult r = QtAndroid::checkPermission("android.permission.ACCESS_NOTIFICATION_POLICY");
       if(r == QtAndroid::PermissionResult::Denied) {
         QtAndroid::requestPermissionsSync( QStringList() << "android.permission.ACCESS_NOTIFICATION_POLICY" );
         qDebug() << "Notify_Policy Denied";
    }
 #else
    qDebug() << "PERMISSIONCONTROLLER: Platform not supported for this action!";
#endif
}
