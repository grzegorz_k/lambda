#ifndef AUTHENTICATIONCONTROLLER_H
#define AUTHENTICATIONCONTROLLER_H

#include <QObject>
#include <QSettings>
#include <QCryptographicHash>
#include <QByteArray>
#include "BaseController.h"

class AuthenticationController : public BaseController
{
    Q_OBJECT
    Q_PROPERTY(bool isAuthenticated READ isAuthenticated WRITE setIsAuthenticated NOTIFY isAuthenticatedChanged)

public:
    explicit AuthenticationController(QSettings *loginSettings,QObject *parent = nullptr);

    QString getEmail() const;
    QString getPassword() const;
    void loadFromQSetting();

signals:
    void isAuthenticatedChanged();

public slots:
    void setIsAuthenticated(bool isAuthenticated);
    void setQSetting(QString email, QString password);
    bool checkEmailAndPassword(QString email,QString password);
    bool isAuthenticated();

private:
    QString _email="l";
    QString _password="o";
    bool _isAuthenticated;
};

#endif // AUTHENTICATIONCONTROLLER_H
