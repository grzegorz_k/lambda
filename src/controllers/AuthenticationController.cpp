#include "AuthenticationController.h"
#include <QDebug>
AuthenticationController::AuthenticationController(QSettings *loginSettings, QObject *parent) : BaseController(loginSettings,parent)
{
  loadFromQSetting();
}

void AuthenticationController::loadFromQSetting()
{
    loginSettings()->beginGroup("login");
    if((_email == loginSettings()->value("email","0").toString())&&
            (QString(QCryptographicHash::hash(_password.toLocal8Bit(),QCryptographicHash::Md5).toHex())) == loginSettings()->value("password","0").toString())
    {
        setIsAuthenticated(true);
    }
    else{
        setIsAuthenticated(false);
    }
    loginSettings()->endGroup();
}

void AuthenticationController::setQSetting(QString email, QString password)
{
    QSettings lastSettings("ScytheStudio","Landa");
    lastSettings.beginGroup("login");
    lastSettings.setValue("email",email);
    lastSettings.setValue("password",QString(QCryptographicHash::hash(password.toLocal8Bit(),QCryptographicHash::Md5).toHex()));
    lastSettings.endGroup();
    emit isAuthenticatedChanged();
}

QString AuthenticationController::getEmail() const
{
    return _email;
}

QString AuthenticationController::getPassword() const
{
    return _password;
}

bool AuthenticationController::checkEmailAndPassword(QString email, QString password)
{
    return _isAuthenticated = email==getEmail() && password==getPassword();
}

bool AuthenticationController::isAuthenticated()
{
    return _isAuthenticated;
}

void AuthenticationController::setIsAuthenticated(bool isAuthenticated)
{
    if (_isAuthenticated == isAuthenticated)
        return;

    _isAuthenticated = isAuthenticated;
    emit isAuthenticatedChanged();
}


