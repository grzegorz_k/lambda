#include "BaseController.h"

BaseController::BaseController(QSettings *loginSettings,QObject *parent) : QObject(parent)
{
    this->_loginSettings = loginSettings;
}

QSettings *BaseController::loginSettings() const
{
    return _loginSettings;
}
