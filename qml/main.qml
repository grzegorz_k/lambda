import QtQuick.Window 2.15
import QtQuick 2.15
import QtQuick.Controls 2.15
import "ui"
import "ui/popup/jobsPopup"
import "ui/popup/loginPage"
import "ui/popup/userPages"
import "ui/general"

ApplicationWindow {
  id: rootId
  width: utilities.dp(480)
  height: utilities.dp(640)
  visible: true
  title: qsTr("Landa")
  property var someBottomMargin: 70

  Utility{
    id: utilities
  }
  ThemeDefinitions{
    id: themeDefinitions
  }
  Connections{
    target: authController
    function onIsAuthenticatedChanged(){
        stackViewId.replaceLoginPage();
    }
  }

  StackView{
    id: stackViewId  
    initialItem: authController.isAuthenticated? mapComponent: loginComponent
    anchors.fill: parent
    function checkAndReplace(comp,name)
    {
      if(stackViewId.depth == 1){
        stackViewId.push(comp,StackView.Immediate);
      }
      else if(stackViewId.currentItem.objectName===name){
        stackViewId.pop(StackView.Immediate);
      }
      else{
        stackViewId.replace(comp,StackView.Immediate);
      }
    }
    function replaceLoginPage(){
      stackViewId.replace(mapComponent,StackView.Immediate);
    }
  }
  Component{
    id: mapComponent
    MainScreen{
      id: map
      objectName: "MAP"
    }
  }
  Component{
    id: loginComponent
    LoginPage{
      id: loginPage
      objectName: "LOGIN"
    }
  }

  DriverFoundPopup{
    id: driverFoundPopup
    height: utilities.dp(220)
    y:parent.height-driverFoundPopup.height-utilities.dp(someBottomMargin)
    width: parent.width
  }

  ShowRoadPopup{
    id: showRoadPopup
    height: utilities.dp(180)
    y: parent.height-showRoadPopup.height-utilities.dp(someBottomMargin)
    width: parent.width
    onEndTrip:{
      rideSummaryPopup.open()
      showRoadPopup.close()
    }
  }

  DriverAcceptPopup{
    id: driverAcceptPopup
    width: parent.width
    height: utilities.dp(350)
    y: parent.height-driverAcceptPopup.height-utilities.dp(someBottomMargin)
    onAccept: {
      driverFoundPopup.open()
      driverAcceptPopup.close()
    }
    onDecline: {
      console.log("decline");
    }
  }

  RideSummary{
    id: rideSummaryPopup
    width: parent.width
    height: utilities.dp(350)
    y: parent.height-rideSummaryPopup.height-utilities.dp(someBottomMargin)
    onLeaveTip: {
      rideSummaryPopup.close()
    }
    onPay: {
      rideSummaryPopup.close()
    }
  }

  Component{
    id: walletComponent
    Wallet{
      id: walletPopup
      objectName: "WALLET"
    }
  }
  Component{
    id: moreComponent
    More{
      id: morePopup
      objectName: "MORE"
    }
  }
  Component{
    id: historyComponent
    History{
      id: historyPopup
      objectName: "HISTORY"
    }
  }

  NavigationBar{
    id: navigationBar
    visible: authController.isAuthenticated
    height: utilities.dp(40)
    width: parent.width
    anchors{
      bottom: parent.bottom
      bottomMargin: utilities.dp(25)
    }

    onHomeClicked: {
      driverAcceptPopup.open();
      if(stackViewId.currentItem.objectName !== "MAP")
        stackViewId.pop(StackView.Immediate);
    }
    onHistoryClicked: {
      stackViewId.checkAndReplace(historyComponent,"HISTORY");
    }
    onMoreClicked: {
      stackViewId.checkAndReplace(moreComponent,"MORE");
    }
    onWalletClicked: {
      stackViewId.checkAndReplace(walletComponent,"WALLET")
    }
  }
}
