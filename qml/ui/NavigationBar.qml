import QtQuick 2.15
import QtQuick.Controls 2.15
import '../ui/general/elements'
import '../../assets/images'
import 'popup/userPages'
Row {
  id:navigationBarRow
  visible: true
  signal homeClicked()
  signal historyClicked()
  signal walletClicked()
  signal moreClicked()
  ImageButton {
    id: homeButton
    width: parent.width/4
    height: utilities.dp(40)
    text: qsTr("HOME")
    imageSource:"qrc:/assets/images/Home.png"
    onClicked:{
      homeClicked();
    }
  }
  ImageButton {
    id: historyButton
    width: parent.width/4
    height: utilities.dp(40)
    text: qsTr("HISTORY")
    imageSource:"qrc:/assets/images/History.png"
    onClicked:{
      historyClicked();
    }
  }
  ImageButton {
    id: walletButton
    width: parent.width/4
    height: utilities.dp(40)
    imageSource:"qrc:/assets/images/Wallet.png"
    text: qsTr("WALLET")
    onClicked:{
      walletClicked();
    }
  }
  ImageButton {
    id: moreButton
    width: parent.width/4
    height: utilities.dp(40)
    imageSource:"qrc:/assets/images/More.png"
    text: qsTr("MORE")
    onClicked:{
      moreClicked();
    }
  }
}
