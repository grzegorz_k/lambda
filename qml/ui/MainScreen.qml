import QtLocation 5.15
import QtPositioning 5.15
import QtQuick 2.15
import QtQuick.Controls 2.15
import "../ui/general/mapElements"
import "../logic"
import "popup/mapPopup"
Page{
  id: mainScreenId
  property double latitude: 52.25
  property double longitude: 21.08
  property bool isPositionFromGPS:  positionId.supportedPositioningMethods != PositionSource.NoPositioningMethods
  Map {
    id:mapId
    anchors.fill: parent
    activeMapType: supportedMapTypes[6]
    center: QtPositioning.coordinate(latitude,longitude)
    zoomLevel: 14

    plugin: Plugin{
      id: mapPlugin
      name: "esri"
      parameters: [
        PluginParameter {
          name: "esri.token"
          value: "usperBQNAiFOOP4i2xCF-D-rcXYi7EgBMYxM5URzVmaiZ8QejVSRImjqQL_cys4PjSgUCvyqeIy4RiJJ77kXBzLYkRZ06CqK17jFsxi4tuwEMbycg2Yi5r6nn5wMVq-pf-sWAG7THRRYJ8dd_QaWpA.."
        },
        PluginParameter {
          name: "esri.mapping.maximumZoomLevel"
          value: 16
        },
        PluginParameter {
          name: "esri.mapping.minimumZoomLevel"
          value: 11
        }
      ]
    }

    MapQuickItem{
      id: currentPositionMarker
      visible: isPositionFromGPS
      coordinate: QtPositioning.coordinate(latitude,longitude)
      sourceItem: Image {
        id: image
        source: "qrc:/assets/images/currentPositionMarker.png"
        width: utilities.dp(35)
        height: utilities.dp(35)
      }
      anchorPoint.x: image.width/2
      anchorPoint.y: image.height/2
    }
  }

  PositionSource{
    id: positionId
    active: authController.isAuthenticated
    preferredPositioningMethods: PositionSource.SatellitePositioningMethods
    onPositionChanged: {
      latitude = position.coordinate.latitude
      longitude = position.coordinate.longitude
    }
    onSourceErrorChanged:{
      permissonController.showGpsPermissionDialog()
    }
    onValidChanged: {
      active = authController.isAuthenticated
    }
    onSupportedPositioningMethodsChanged: {
      if(positionId.supportedPositioningMethods == PositionSource.NoPositioningMethods){
        gpsNotEnablePopup.open()
      }
      else{
        gpsNotEnablePopup.close()
        mapId.center = QtPositioning.coordinate(latitude,longitude)
      }
    }
  }
  GPSButton{
    id: buttonGPS
    visible: isPositionFromGPS
    anchors{
      top:  parent.top
      right: parent.right
      rightMargin: utilities.dp(40)
      topMargin: utilities.dp(30)
    }
    onClicked: {
      mapId.center = QtPositioning.coordinate(latitude,longitude)
      mapId.zoomLevel = 16
    }
  }
  ErrorPopup{
    id: gpsNotEnablePopup
    width: parent.width
    height: utilities.dp(130)
    y: parent.height/2-gpsNotEnablePopup.height/2
    onError: {
      gpsNotEnablePopup.close()
    }
  }
}
