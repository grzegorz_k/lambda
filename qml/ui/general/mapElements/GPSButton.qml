import QtQuick 2.15
import "../elements"

ImageButton{
  id: idGPS
  width: utilities.dp(40)
  height: utilities.dp(40)
  imageSource:'qrc:/assets/images/GPS.png'
  text: "My Position"
}
