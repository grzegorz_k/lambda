import QtQuick 2.15

Item {
  id: root

  readonly property string borderColor: "cyan"
  readonly property string driverInformationBackgroundColor: "beige"
}
