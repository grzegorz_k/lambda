import QtQuick 2.15

Text {
  id: appTextId
  property var appText: "text"
  property var fontSize: 12
  text: appText
  font.pixelSize: fontSize
}
