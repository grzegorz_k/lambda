import QtQuick 2.15
import QtQuick.Controls 2.15

Item {
  id: imagebutton

  property alias text:textId.text
  property alias imageSource: image.source
  signal clicked()
  Image {
    id: image
    anchors.fill: parent
    source: "qrc:/assets/images/basicButton.png"
    fillMode: Image.PreserveAspectFit
    sourceSize {
      height: 40
      width: 40
    }
    smooth: true
  }
  Text{
    id:textId
    anchors{
      top: image.bottom
      horizontalCenter: image.horizontalCenter
      topMargin: utilities.dp(5)
    }
    text: qsTr("button")
  }
  MouseArea {
    id: loginButtonMouseArea
    anchors.fill: parent
    onClicked: {
      imagebutton.clicked()
    }
  }
}


