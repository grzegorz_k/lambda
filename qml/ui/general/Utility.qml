import QtQuick 2.15
import QtQuick.Window 2.15

QtObject {
  /*
    Function for calulating desity independet pixels.
    Desired logical size is passed as parameter, then it is multiplied by pixel density of the screen.
    Due to pixel desity unit (dots per milimeter) value needs to be multiplied by 25.4 to get
    number of dots per inch. After that value needs to be devided by 160 which is standard screen desity.
    */
  function dp(pixels) {
    return pixels * Screen.pixelDensity * 25.4 / 160
  }
}

