import QtQuick.Controls 2.15
import QtQuick 2.15
import '../../../../assets/logo/'
import '../../general/elements'
Page{
  id : loginPage
  property var textFieldHeight: 50
  property var textFieldWidth: 360
  Image{
    id: logoImage
    anchors{
      top: parent.top
      right: parent.right
      left: parent.left
      rightMargin: utilities.dp(20)
      leftMargin: utilities.dp(20)
      topMargin: utilities.dp(75)
    }
    source:'qrc:/assets/logo/LandaLogo.png'
    fillMode: Image.PreserveAspectFit
  }
  TextField{
    id: emailField
    height: utilities.dp(textFieldHeight)
    width: utilities.dp(textFieldWidth)
    anchors{
      top: logoImage.bottom
      horizontalCenter: parent.horizontalCenter
      topMargin: utilities.dp(60)
    }
    placeholderText:qsTr("Email")  
  }
  TextField{
    id: passwordField
    height: utilities.dp(textFieldHeight)
    width: utilities.dp(textFieldWidth)
    anchors{
      top: emailField.bottom
      horizontalCenter: parent.horizontalCenter
      topMargin: utilities.dp(20)
    }
    placeholderText:qsTr("Password")
    echoMode: TextField.Password
  }
  Label{
    id:errorLabel
    visible: false
    anchors{
      top: passwordField.bottom
      right: passwordField.right
      topMargin: utilities.dp(5)
    }
    text: qsTr("Bad email or password")
  }

  AppButton{
    id: button
    width: utilities.dp(120)
    height: utilities.dp(60)
    anchors{
      top: passwordField.bottom
      horizontalCenter: parent.horizontalCenter
      topMargin: utilities.dp(40)
    }
    text: "Sign in"
    onClicked: {
      if(authController.checkEmailAndPassword(emailField.text,passwordField.text))
      {
        authController.setQSetting(emailField.text,passwordField.text);
      }
      else{
        errorLabel.visible=true;
        emailField.text=''
        passwordField.text=''
      }
    }
  }
}
