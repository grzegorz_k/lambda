import QtQuick 2.15
import QtQuick.Controls 2.15
import '../../general/elements'
import '../jobsPopup/elements/'

Popup{
  id: errorPopup
  closePolicy: "NoAutoClose"
  signal error()

  background: MainRectangle {
    id: errorMessage
    anchors{
      fill: parent
      leftMargin: utilities.dp(70)
      rightMargin: utilities.dp(70)
    }
    text: qsTr("Please turn on GPS")
    _fontSize: 24
    _horizontalAlignment: Text.AlignHCenter
  }
  AppButton {
    id: endButton
    height: utilities.dp(50)
    width: utilities.dp(200)
    anchors{
      horizontalCenter: parent.horizontalCenter
      bottom: parent.bottom
      bottomMargin: utilities.dp(5)
    }
    text: qsTr("Continue without GPS")
    onClicked: {
      error()
    }
  }

}
