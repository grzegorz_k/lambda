import QtQuick 2.15
import QtQuick.Controls 2.15
import '../../general/elements'
import 'elements/'
Popup{
  id: driverAcceptPopup
  closePolicy: "NoAutoClose"
  property var fontPixelSize: 12
  signal accept()
  signal decline()

  background: MainRectangle{
    id: driverAcceptMainRectangle
    anchors{
      fill: parent
      leftMargin: utilities.dp(25)
      rightMargin: utilities.dp(25)
    }
    text: qsTr("Hey! John wants to ride with you")

    DriverHeaderInformation{
      id: driverHeaderInformation
      anchors{
        top: parent.top
        left: parent.left
        right: parent.right
        rightMargin: utilities.dp(5)
        leftMargin: utilities.dp(5)
        topMargin: utilities.dp(15)
      }
      upperText: "22.3$"
      bottonText: "12.3km"
    }

    AppText {
      id: pickupText
      anchors{
        left: parent.left
        top: parent.top
        topMargin: utilities.dp(130)
        leftMargin: utilities.dp(80)
      }
      appText: qsTr("Pickup point")
      fontSize: fontPixelSize
    }

    AppText {
      id: pickoutText
      anchors{
        left: parent.left
        top: startLocalizationText.bottom
        topMargin: utilities.dp(40)
        leftMargin: utilities.dp(80)
      }
      appText: qsTr("Pickout point")
      fontSize: fontPixelSize
    }

    AppText {
      id: destinationLocalizationText
      anchors{
        left: parent.left
        top: pickoutText.bottom
        leftMargin: utilities.dp(80)
        topMargin: utilities.dp(10)
      }
      appText: qsTr("WAT")
      fontSize: fontPixelSize
    }

    AppText {
      id: startLocalizationText
      anchors{
        left: parent.left
        top: pickupText.bottom
        leftMargin: utilities.dp(80)
        topMargin: utilities.dp(10)
      }
      appText: qsTr("Zielonka")
      fontSize: fontPixelSize
    }

    Image {
      id: pickupImage
      width: utilities.dp(20)
      height: utilities.dp(20)
      anchors{
        left: parent.left
        top: pickupText.top
        leftMargin: utilities.dp(50)
      }
      fillMode: Image.PreserveAspectFit
      source: "qrc:/assets/images/Black_circle.png"
    }

    Image {
      id: pickoutImage
      width: utilities.dp(20)
      height: utilities.dp(20)
      anchors{
        left: parent.left
        top: pickoutText.top
        leftMargin: utilities.dp(50)
      }
      fillMode: Image.PreserveAspectFit
      source: "qrc:/assets/images/marker.png"
    }
    AppButton {
      id: declineButton
      height: utilities.dp(40)
      width: parent.width/2-utilities.dp(35)
      anchors{
        left: parent.left
        bottom: parent.bottom
        leftMargin: utilities.dp(25)
        bottomMargin: utilities.dp(45)
      }
      text: qsTr("Decline")
      onClicked: {
        driverAcceptPopup.decline();
      }
    }

    AppButton {
      id: acceptButton
      height: utilities.dp(40)
      anchors{
        right: parent.right
        bottom: parent.bottom
        left: declineButton.right
        leftMargin: utilities.dp(20)
        rightMargin: utilities.dp(25)
        bottomMargin: utilities.dp(45)
      }
      text: qsTr("Accept")
      onClicked: {
        driverAcceptPopup.accept();
      }
    }
  }
}
