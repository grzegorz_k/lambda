import QtQuick 2.15
import QtQuick.Controls 2.15
import '../../general/elements'
import 'elements/'
Popup{
  id: rideSummaryPopup
  closePolicy: "NoAutoClose"
  signal pay()
  signal leaveTip()

  background: MainRectangle {
    id: rideSummary
    anchors{
      fill: parent
      leftMargin: utilities.dp(25)
      rightMargin: utilities.dp(25)
    }
    text: qsTr("Ride summary and payment")
  }

  Row{
    id:buttonsRow
    visible: true
    height:utilities.dp(40)
    anchors{
      bottom: parent.bottom
      left: parent.left
      right: parent.right
      leftMargin: parent.width/8
      rightMargin: parent.width/8
      bottomMargin: utilities.dp(40)
    }
    spacing: utilities.dp(4)

    AppButton{
      id: paymentButton
      width: parent.width/2
      height: utilities.dp(40)
      text: qsTr("Pay")
      onClicked: {
        rideSummaryPopup.pay()
      }
    }

    AppButton{
      id:tipButton
      width: parent.width/2
      height: utilities.dp(40)
      text: qsTr("Leave a tip")
      onClicked: {
        rideSummaryPopup.leaveTip();
      }
    }
  }
}
