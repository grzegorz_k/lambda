import QtQuick 2.15
import '../../../general/elements'

Rectangle{
  id: driverInformationPopup
  radius: 30
  Image {
    id: acceptedImage
    width: utilities.dp(30)
    height: utilities.dp(30)
    anchors{
      right: ratingImage.left
      top: parent.top
      rightMargin: utilities.dp(80)
      topMargin: utilities.dp(25)
    }
    fillMode: Image.PreserveAspectFit
    sourceSize.height: 30
    sourceSize.width: 30
    smooth: true
    source: "qrc:/assets/images/acceptCircle.png"
  }

  Image {
    id: ratingImage
    width: utilities.dp(30)
    height: utilities.dp(30)
    anchors{
      horizontalCenter: parent.horizontalCenter
      top: parent.top
      topMargin: utilities.dp(25)
    }
    fillMode: Image.PreserveAspectFit
    sourceSize.height: 30
    sourceSize.width: 30
    smooth: true
    source: "qrc:/assets/images/ratingCircle.png"
  }

  Image {
    id: cancelledImage
    width: utilities.dp(30)
    height: utilities.dp(30)
    anchors{
      left: ratingImage.right
      top: parent.top
      topMargin: utilities.dp(25)
      leftMargin: utilities.dp(80)
    }
    fillMode: Image.PreserveAspectFit
    sourceSize.height: 30
    sourceSize.width: 30
    smooth: true
    source: "qrc:/assets/images/cancelCircle.png"
  }

  AppText {
    id: acceptedText
    anchors{
      horizontalCenter: acceptedImage.horizontalCenter
      top: acceptedImage.bottom
      topMargin: utilities.dp(5)
    }
    appText: qsTr("Accepted")
  }

  AppText {
    id: ratingText
    anchors{
      horizontalCenter: ratingImage.horizontalCenter
      top: ratingImage.bottom
      topMargin: utilities.dp(5)
    }
    appText: qsTr("Rating")
  }

  AppText {
    id: cancelledText
    anchors{
      horizontalCenter: cancelledImage.horizontalCenter
      top: cancelledImage.bottom
      topMargin: utilities.dp(5)
    }
    appText: qsTr("Cancelled")
  }
}

