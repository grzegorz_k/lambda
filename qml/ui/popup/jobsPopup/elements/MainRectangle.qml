import QtQuick 2.15
import QtQuick.Controls 2.15
import '../../../general/elements'
  Rectangle{
    id: basicDialog
    border.color: themeDefinitions.borderColor
    radius: 30
    property alias text: header.text
    property var _fontSize: 15
    property var _horizontalAlignment: Text.AlignLeft
    AppText {
      id: header
      anchors{
        left: parent.left
        right: parent.right
        top: parent.top
        leftMargin: utilities.dp(20)
        rightMargin: utilities.dp(20)
        topMargin: utilities.dp(15)
      }
      appText: qsTr("Header")
      fontSize: _fontSize
      horizontalAlignment: _horizontalAlignment
    }
  }

