import QtQuick 2.0
import '../../../general/elements'
Item {
  id: headerId
  property var fontPixelSize: 13
  property alias upperText: upperTextId.text
  property alias bottonText: bottonTextId.text

  Image {
    id: driverImage
    width: utilities.dp(50)
    height: utilities.dp(50)
    anchors{
      left: parent.left
      top: parent.top
      leftMargin: utilities.dp(40)
      topMargin: utilities.dp(40)
    }
    fillMode: Image.PreserveAspectFit
    source: "qrc:/assets/images/linda.png"
  }

  AppText {
    id: driverName
    anchors{
      left: driverImage.right
      top: parent.top
      leftMargin: utilities.dp(5)
      topMargin: utilities.dp(40)
    }
    appText: qsTr("John")
    fontSize: fontPixelSize+4
  }

  AppText {
    id: driverLevel
    anchors{
      left: driverImage.right
      top: driverName.bottom
      topMargin: utilities.dp(10)
      leftMargin: utilities.dp(5)
    }
    appText: qsTr("Basic level")
    fontSize: fontPixelSize
  }

  AppText {
    id: upperTextId
    anchors{
      top: parent.top
      right: parent.right
      rightMargin: utilities.dp(40)
      topMargin: utilities.dp(40)
    }
    appText: qsTr("22.3$")
    fontSize: fontPixelSize+5
  }

  AppText {
    id: bottonTextId
    anchors{
      top: upperTextId.bottom
      left: upperTextId.left
      topMargin: utilities.dp(10)
    }
    appText: qsTr( "12.3km")
    fontSize: fontPixelSize
  }
}
