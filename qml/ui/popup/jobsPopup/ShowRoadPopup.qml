import QtQuick 2.15
import QtQuick.Controls 2.15
import '../../general/elements'
import 'elements/'

Popup{
  id: showRoadPopup
  closePolicy: "NoAutoClose"
  property var headerFontSize: 14
  property var contentFontSize: 20
  signal endTrip()

  background: MainRectangle {
    id: showRoadRectangle
    anchors{
      fill: parent
      leftMargin: utilities.dp(25)
      rightMargin: utilities.dp(25)
    }

    AppText {
      id: paymentType
      anchors{
        left: parent.left
        top: parent.top
        right: cash.left
        leftMargin: utilities.dp(60)
        topMargin: utilities.dp(40)
        rightMargin: utilities.dp(60)
      }
      appText: qsTr("Payment Type")
      fontSize: headerFontSize
    }
    AppText {
      id: promo
      anchors{
        left: paymentType.left
        right: paymentType.right
        top: paymentType.top
        topMargin: utilities.dp(30)
      }
      appText: qsTr("Promo")
      fontSize: contentFontSize
    }

    AppText {
      id: cash
      anchors{
        left: paymentType.left
        right: parent.right
        top: parent.top
        leftMargin: utilities.dp(140)
        rightMargin: utilities.dp(150)
        topMargin: utilities.dp(40)
      }
      appText: qsTr("Cash")
      fontSize: headerFontSize
    }

    AppText {
      id: percent
      anchors{
        left: cash.left
        top: cash.top
        topMargin: utilities.dp(30)
      }
      appText: qsTr("15% Applied")
      fontSize: contentFontSize
    }

    AppButton {
      id: endButton
      height: utilities.dp(50)
      width: utilities.dp(200)
      anchors{
        horizontalCenter: parent.horizontalCenter
        bottom: parent.bottom
        bottomMargin: utilities.dp(20)
      }
      text: qsTr("End Trip")
      onClicked: {
        showRoadPopup.endTrip();
      }
    }
  }
}
