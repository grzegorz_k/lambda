import QtQuick 2.15
import QtQuick.Controls 2.15
import '../../general/elements'
import 'elements/'
Popup{
  id: driverFoundPopup
  closePolicy: "NoAutoClose"

  background: MainRectangle{
    id: driverMainRectangle
    text: "15%+"
    anchors{
      fill: parent
      leftMargin: utilities.dp(25)
      rightMargin: utilities.dp(25)
    }
    DriverHeaderInformation{
      id: driverHeaderInformation
      anchors{
        top: parent.top
        left: parent.left
        right: parent.right
        rightMargin: utilities.dp(5)
        leftMargin: utilities.dp(5)
        topMargin: utilities.dp(5)
      }
      upperText: "535,54$"
      bottonText: "balance"
    }
    DriverInformation{
      id: drivertInformation

      height: utilities.dp(100)
      anchors{
        left: parent.left
        right: parent.right
        bottom: parent.bottom
        rightMargin: utilities.dp(15)
        leftMargin: utilities.dp(15)
        bottomMargin: utilities.dp(15)
      }
      border.color: themeDefinitions.borderColor
      color: themeDefinitions.driverInformationBackgroundColor
    }
  }
}

